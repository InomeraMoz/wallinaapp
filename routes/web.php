<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Minhas rotas

Route::get('/admin', 'AuthController@dashbord')->name('admin');
Route::get('/', 'AuthController@showforLogin')->name('admin.login');
Route::get('/admin/logout', 'AuthController@logout')->name('admin.logout');
Route::post('/admin/login/do', 'AuthController@login')->name('admin.login.do');

//Cliente Route
Route::get('/admin/cliente/clientes', 'indexController@getCliente')->name('admin.cliente.clientes');
Route::get('admin/Empresa/pegarCliente/{id}', 'crudController@pegarIdCliente')->name('admin.cliente.idCliente');
Route::get('admin/Empresa/pegarCliente_actualizacao/{id}', 'crudController@pegarIdCliente_para_actualizacao')->name('admin.cliente.idCliente_actualizacao');
Route::get('/admin/cliente/novo', 'indexController@getnovoCliente')->name('admin.cliente.novo');
Route::post('/admin/cliente/save', 'crudController@criarCliente')->name('admin.cliente.save');
Route::post('/admin/cliente/update/{id}', 'crudController@updateCliente')->name('admin.cliente.update');

Route::get('/admin/cliente/empresas', 'indexController@getEmpresa')->name('admin.cliente.empresa');
Route::get('/admin/cliente/empresas/pegarEmpresa_porID/{id}', 'indexController@getEmpresabyId')->name('admin.cliente.empresa.pegarEmpresa_id');
Route::get('/admin/cliente/empresas/nova', 'indexController@getnovaEmpresa')->name('admin.cliente.empresas.nova');
Route::get('/admin/Cliente/Empresa/save', 'crudController@criarEmpresa')->name('admin.cliente.empresa.save');
Route::post('/admin/Cliente/Empresa/update/{id}', 'crudController@updateEmpresa_id')->name('admin.cliente.empresa.update_id');
Route::get('/admin/Cliente/Empresa2/save', 'crudController@criarEmpresa2')->name('admin.cliente.empresa2.save');
Route::get('admin/Cliente/Empresa/contaEmpresa', 'indexController@getContaUsuario')->name('admin.cliente.empresa.contautilizador.nova');
Route::get('admin/Cliente/Empresa/pegarEmpresa/{id}', 'indexController@pegarIdEmpresa')->name('admin.cliente.empresa.contautilizador.idEmpresa.nova');
Route::get('admin/Cliente/Empresa/contas_Empresas', 'indexController@getContasEmpresas')->name('admin.Cliente.Empresa.conta_empresa');
Route::get('admin/Cliente/Empresa/contaUtilizador/nova', 'crudController@criarContaUtilizador2')->name('admin.cliente.empresa.utilizador.novo_');
Route::post('admin/cliente/empresa/conta_utilizador/nova', 'crudController@criarContaUtilizador')->name('admin.cliente.empresa.conta_utilizador.nova');
Route::get('admin/cliente/empresa/conta_utilizador/pegarId_User/{id}', 'indexController@pegar_id_Utilizador')->name('admin.cliente.empresa.conta_utilizador.catch_by_id');
Route::post('admin/cliente/empresa/conta_utilizador/update/{id}', 'crudController@editarContaUtilizador')->name('admin.cliente.empresa.conta_utilizador.update');


// Rotas para gestão de clientes

//Caixa
Route::get('/user/Caixa/TodasCaixas', 'indexController@getcaixa')->name('user.caixa.caixas');
Route::get('/user/caixa/novo', 'indexController@getnewCaixa')->name('user.caixa.nova');
Route::post('/user/caixa/abrirCaixa', 'crudController@abrirCaixa')->name('user.caixa.abrirCaixa');
Route::get('/user/caixa/pegarid_caixa/{id}', 'indexController@pegarId_Caixa')->name('user.caixa.pegar_idCaixa');
Route::post('/user/caixa/pegarid_caixa/update/{id}', 'indexController@update_Caixa')->name('user.caixa.pegar_idCaixa.update');
Route::get('/user/caixa/feixo', 'indexController@getfeixoCaixa')->name('user.caixa.feixo');

// Routas da  configuração

Route::get('/user/configuracao/detalhesPadrao1', 'indexController@getDetalhesPadrao1')->name('user.configuracao.detalhesPadrao1');
Route::get('/user/configuracao/novoPadrao1', 'indexController@getnovoPadrao1')->name('user.configuracao.novoPadrao1');
Route::post('/user/configuracao/novoPadrao1/gravar', 'crudController@gravarPadrao1')->name('user.configuracao.novoPadrao1.gravar');
Route::get('/user/configuracao/novoPadrao1/pegar_id/{id}', 'indexController@pegarnovoPadrao1')->name('user.configuracao.novoPadrao1.pegar_id');
Route::post('/user/configuracao/novoPadrao1/update/{id}', 'crudController@updatePadrao1')->name('user.configuracao.novoPadrao1.update');
Route::get('/user/configuracao/detalhesPadrao2', 'indexController@getDetalhesPadrao2')->name('user.configuracao.detalhesPadrao2');
Route::get('/user/configuracao/detalhesPadrao2/pegar_id/{id}', 'indexController@pegarnovoPadrao2')->name('user.configuracao.detalhesPadrao2.pegar_id');
Route::get('/user/configuracao/novoPadrao2', 'indexController@getnovoPadrao2')->name('user.configuracao.novoPadrao2');
Route::post('/user/configuracao/novoPadrao2/gravar', 'crudController@gravarPadrao2')->name('user.configuracao.novoPadrao2.gravar');
Route::post('/user/configuracao/novoPadrao2/update/{id}', 'crudController@updatePadrao2')->name('user.configuracao.novoPadrao2.update');
Route::get('/user/configuracao/detalhesTipo', 'indexController@getDetalhesTipo')->name('user.configuracao.detalhesTipo');
Route::get('/user/configuracao/novoTipo', 'indexController@getnovoTipo')->name('user.configuracao.novoTipo');
Route::get('/user/configuracao/tipoProduto/pegar_id/{id}', 'indexController@pegarId_tipoProduto')->name('user.configuracao.tipoProduto.pegarId');
Route::post('/user/configuracao/tipoProduto/update/{id}', 'crudController@updatetipoProduto')->name('user.configuracao.tipoProduto.update');
Route::post('/user/configuracao/tipoProduto/create', 'crudController@criarTipoProduto')->name('user.configuracao.tipoProduto.create');

//fim das routas de configuração

// Routas de produtos

Route::get('user/Prduto/produtos', 'indexController@getProdutos')->name('user.Prduto.produtos');
Route::get('user/Prduto/novo', 'indexController@getnovoProduto')->name('user.Prduto.novo');
Route::post('user/Prduto/save', 'crudController@criarProduto')->name('user.Prduto.save');
Route::get('user/Prduto/pegar_id/{id}', 'indexController@pegar_id_produto')->name('user.Prduto.pegar_id');
Route::post('user/Prduto/update/{id}', 'crudController@updateProdutos')->name('user.Prduto.update');


// Routas de receitas e dispesas
//Receitas
Route::get('/user/dispesas&receitas/receitas/receitas', 'indexController@getdetalhesReceitas')->name('user.dispesas&receitas.receitas.todas');
Route::get('/user/dispesas & receitas/receitas/nova', 'indexController@getReceitas')->name('user.dispesas_receitas.receitas.nova');
Route::post('/user/dispesas&receitas/receitas/save', 'crudController@gravarReceitas')->name('user.dispesas&receitas.receitas.save');
Route::get('/user/dispesas&receitas/receitas/pegar_id/{id}', 'indexController@pegar_id_receitas')->name('user.dispesas&receitas.receitas.pegar_id');
Route::post('/user/dispesas&receitas/receitasupdate/{id}', 'crudController@updateReceitas')->name('user.dispesas&receitas.receitas.update');

//Dispesas
Route::get('/user/dispesas&receitas/dispesas/dispesas', 'indexController@getdetalhesDispesas')->name('user.dispesas&receitas.dispesas.todas');
Route::get('/user/dispesas&receitas/dispesas/nova', 'indexController@getDispesas');
Route::post('/user/dispesas&receitas/dispesas/save', 'crudController@gravarDespesas')->name('user.dispesas&receitas.dispesas.save');
Route::get('/user/dispesas&receitas/dispesas/pegar_id/{id}', 'indexController@pegar_id_dispesas')->name('user.dispesas&receitas.dispesas.pegar_id');
Route::post('/user/dispesas&receitas/dispesas/update/{id}', 'crudController@updateDispesas')->name('user.dispesas&receitas.dispesas.update');

// Metodo para adicionar as entradas e saida de produtos

Route::get('/user/Entradas&Saidas/Entradas/entradas', 'indexController@getEntradas')->name('user.Entradas&Saidas.Entradas.entradas');
Route::get('/user/Entradas&Saidas/Entradas/EntradaProduto', 'indexController@getEntradaProduto')->name('user.Entradas&Saidas.Entradas.EntradaProduto');
Route::post('/user/Entradas&Saidas/Entradas/saveEntradaProduto', 'crudController@createEntradaProduto')->name('user.Entradas&Saidas.Entradas.saveEntradaProduto');


Route::get('/user/Entradas&Saidas/Saidas/saidas', 'indexController@getSaidas')->name('user.Entradas&Saidas.Saidas.saidas');
Route::get('/user/Entradas&Saidas/Saidas/saidaProduto', 'indexController@getSaidaProduto')->name('user.Entradas&Saidas.Saidas.saidaProduto');
Route::post('/user/Entradas&Saidas/Saidas/saveSaidaProduto', 'crudController@createSaidaProduto')->name('user.Entradas&Saidas.Saidas.saveSaidaProduto');

// Metodos do stoque, para acessar os controllers do stoque 

Route::get('/user/Produtos/Stoque/produtos_no_stoque', 'indexController@getprodutosStoque')->name('user.Produtos.Stoque.produtos_no_stoque');
