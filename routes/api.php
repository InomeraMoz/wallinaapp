<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/allbox', 'Caixa\caixaController@index');
Route::get('/findboxId/{id}', 'apiRequestController@api_findcaixa');
Route::POST('/sevebox', 'apiRequestController@api_sevebox');
Route::put('/updatebox/{caixa}', 'apiRequestController@api_updatebox');
Route::POST('/createUser', 'apiRequestController@api_sevebox');
Route::POST('/login', 'api\apiRequestController@login');
