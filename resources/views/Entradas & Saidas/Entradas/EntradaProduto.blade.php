@extends('index')
@section('styleshet')
<link rel="stylesheet" href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css">
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css">
<link rel="stylesheet" href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
<link rel="stylesheet" href="assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
<link rel="stylesheet" href="assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Produto<small>Formulario do Registo de Entrada de produto</small></h1>
  </div>
</div>
@stop

@section('conteudo')
<!-- Select2  -->
<!-- Main content -->

<!-- Main content -->
@section('informacao1')
<div class="col-sm-6">
  <h1 class="m-0 text-dark">Registo entrada de produto</h1>
</div><!-- /.col -->
@stop
@section('informacao2')
<li class="breadcrumb-item active">Entrada de Produto</li>
@stop

<!-- SELECT2 EXAMPLE -->
<section>
  <form action="/saveEntradaProduto">
    <div class="box box-default">
      <div class="box-body">
        <div class="panel-heading">
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">

              <div class="form-group">
                <label>Data da Entrada:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <span data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">

              <div class="form-group">
                <label>Lista de todos produtos</label>
                <select id="Produtos_id" class="form-control search-select" style="width: 100%;" name="Produtos_id">
                  <option value="">--Selecione--</option>
                  @foreach($produtos as $produtos)
                  <option value="{{$produtos->id}}">{{$produtos->Designacao}} - {{$produtos->subDesignacao}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <!-- /.form-group -->
          </div>

          <div class="col-md-3">
            <div class="form-group">

              <div class="form-group">
                <label>Validade do produto/item:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input data-date-format="yyyy-mm-dd" id="dataValidade" data-date-viewmode="years" class="form-control date-picker" name="validade">
                </div>
              </div>
            </div>
          </div>


          <!-- /.col -->
          <div class="col-md-2">

            <div class="form-group">
              <label>Quantidade:</label>
              <div class="input-group">
                <input type="number" id="quantidade" class="form-control" name="quantidade">
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <!-- /.form-group -->
          </div>

          <div class="col-md-3">
            <!-- /.form-group -->
            <div class="form-group">
              <label>Valor:</label>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" id="precoCompra" class="form-control" name="precoCompra">
                <span class="input-group-addon">.00</span>
              </div>
            </div>


          </div>
          <!-- /.col -->
        </div>
        <!-- /.form-group -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.box-body -->
    <a id="addEntra" name="addEntra" class="btn btn-info pull-right"><i class="fa fa-fw fa-cart-plus"></i>ADD</a>
    </div>
    <!-- /.box -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Table old out items</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="exampleEntrada" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Referencia</th>
                  <th>Designação do produto</th>
                  <th>Validade</th>
                  <th>Quantidade</th>
                  <th>Preço</th>
                  <th>Total</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

              </tbody>
              <!-- <tfoot>
                </tfoot> -->
            </table>
          </div>


          <!-- /.box-body -->
        </div>
        <div class="col-md-3 pull-right">
          <!-- /.form-group -->
          <div class="form-group">
            <label>Cash:</label>
            <div class="input-group">
              <span class="input-group-addon">$</span>
              <input type="text" id="totalPagar" class="form-control input-lg">
              <span class="input-group-addon">MZN</span>
            </div>
            <input type="text" id="listaDados" name="listaDados">
          </div>


        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

      <button type="submit" class="btn btn-info pull-right"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
    </div>
    <!-- /.row -->
  </form>
</section>
<!-- /.content -->
@stop

@section('scripts')

<script>
  var totalAPagar = 0;
  var carinhaCompras = [];
  var elementosTabela = "";

  function pegardados() {
    $('#addEntra').on('click', function() {
      carinhaCompras.push({
        "Produtos_id": $("#Produtos_id").val(),
        "designacao": $("#Produtos_id option:selected").text(),
        "dataValidade": $("#dataValidade").val(),
        'quantidade': $("#quantidade").val(),
        'precoCompra': $("#precoCompra").val()
      });
      $("#exampleEntrada").append($('<tr>')
        .append($('<td>').append($("#Produtos_id").val()))
        .append($('<td>').append($("#Produtos_id option:selected").text()))
        .append($('<td>').append($("#dataValidade").val()))
        .append($('<td>').append($("#quantidade").val()))
        .append($('<td>').append($("#precoCompra").val()))
        .append($('<td name="total">').append($("#precoCompra").val() * $("#quantidade").val()))
      )
      totalAPagar += $("#precoCompra").val() * $("#quantidade").val()
      $("#totalPagar").val(totalAPagar);
      var elementosTabela = "";
      for (i = 0; i < carinhaCompras.length; i++) {
        elementosTabela += carinhaCompras[i].Produtos_id + "|" + carinhaCompras[i].designacao + "|" +
          carinhaCompras[i].dataValidade + "|" + carinhaCompras[i].quantidade + "|" +
          carinhaCompras[i].precoCompra + ";"
      }
      $("#listaDados").val(JSON.stringify(elementosTabela))

    })
  }

  jQuery(document).ready(function() {

    FormElements.init();
    pegardados();
    $("#totalPagar").val(totalAPagar);
  });
</script>
@stop
@stop