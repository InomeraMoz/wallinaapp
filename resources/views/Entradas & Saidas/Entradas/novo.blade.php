@extends('index')

@section('stylesheet')
<link rel="stylesheet" href="assets/css/styles.css">
<link rel="stylesheet" href="assets/css/styles-responsive.css">
<link rel="stylesheet" href="assets/css/plugins.css">
<link rel="stylesheet" href="assets/css/themes/theme-style8.css" type="text/css" id="skin_color">
<link rel="stylesheet" href="assets/css/print.css" type="text/css" media="print" />
@stop

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Data Tables <small>Formulario do caixa</small></h1>
  </div>
</div>
@stop
@section('informacao_comp')
<ol class="breadcrumb">
  <li>
    <a href="#">
      Dashboard
    </a>
  </li>
  <li class="active">
    Registo do caixa
  </li>
</ol>
@stop
@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-white">
      <div class="panel-heading">
      </div>
      <div class="panel-body">
        
        {{$listaDados}}

        
        
      </div>
    </div>
  </div>
</div>

@section('scripts')

<script>
  jQuery(document).ready(function() {
    Main.init();
    SVExamples.init();
    FormElements.init();
  });
</script>
@stop
@stop