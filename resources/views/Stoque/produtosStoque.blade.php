@extends('Dashbord')
@section('stylesheet')


@section('informacao1')
<div class="col-sm-6 hidden-xs">
    <div class="page-header">
        <h1>Tables de Dados <small>Tabela de Todos produtos do stoque</small></h1>
    </div>
</div>
@stop
@section('informacao_comp')
<ol class="breadcrumb">
    <li>
        <a href="#">
            Dashboard
        </a>
    </li>
    <li class="active">
        Tabelas de todos produtos no stoque
    </li>
</ol>
@stop
@section('conteudo')
<div class="row">
    <div class="col-sm-12">
        <!-- start: ACCORDION PANEL -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title">Meu Stoque</h4>
                <div class="panel-tools">
                    <div class="dropdown">
                        <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
                            <i class="fa fa-cog"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-light pull-right" role="menu">
                            <li>
                                <a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
                            </li>
                            <li>
                                <a class="panel-refresh" href="#"> <i class="fa fa-refresh"></i> <span>Refresh</span> </a>
                            </li>
                            <li>
                                <a class="panel-config" href="#panel-config" data-toggle="modal"> <i class="fa fa-wrench"></i> <span>Configurations</span></a>
                            </li>
                            <li>
                                <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span></a>
                            </li>
                        </ul>
                    </div>
                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="panel-group accordion" id="accordion">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <i class="icon-arrow"></i> Meu stoque -> produtos no stoque #1
                                </a></h5>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>Reference receita</th>
                                                <th>Nome do produto</th>
                                                <th>Tipo de Produto</th>
                                                <th>Categoria</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($produtos as $pr)

                                            <tr>


                                                <td>{{$pr->id_stoque}}</td>
                                                <td>{{$pr->Designacao}}</td>
                                                <td>{{$pr->designacao_padrao}}</td>
                                                <td>{{$pr->tipoproduto}}</td>
                                                <td>{{$pr->quantidade}}</td>
                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                                                        <a href="" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Reference receita</th>
                                                <th>Nome do produto</th>
                                                <th>Tipo de Produto</th>
                                                <th>Categoria</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    <i class="icon-arrow"></i> Meu Stoque-> Baixo ou roptura #2
                                </a></h5>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>Reference receita</th>
                                                <th>Nome do produto</th>
                                                <th>Tipo de Produto</th>
                                                <th>Categoria</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($produtos as $pr)

                                            <tr>
                                                @if($pr->quantidade <= 155) <td>{{$pr->id_stoque}}</td>
                                                    <td>{{$pr->Designacao}}</td>
                                                    <td>{{$pr->designacao_padrao}}</td>
                                                    <td>{{$pr->tipoproduto}}</td>
                                                    <td>{{$pr->quantidade}}</td>
                                                    <td>
                                                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                                                            <a href="" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                        </div>
                                                    </td>
                                                    @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Reference receita</th>
                                                <th>Nome do produto</th>
                                                <th>Tipo de Produto</th>
                                                <th>Categoria</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                    <i class="icon-arrow"></i> Collapsible Group Item #3
                                </a></h5>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>Reference receita</th>
                                                <th>Nome do produto</th>
                                                <th>Tipo de Produto</th>
                                                <th>Categoria</th>
                                                <th>Validade</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($entradas as $en)
                                            <tr>
                                                <td>{{$en->id}}</td>
                                                <td>{{$en->Designacao}}</td>
                                                <td>{{$en->designacao_padrao}}</td>
                                                <td>{{$en->tipoproduto}}</td>
                                                <td>{{$en->validade}}</td>
                                                <td>{{$en->quantidade}}</td>
                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                                                        <a href="" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                    </div>
                                                </td>

                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Reference receita</th>
                                                <th>Nome do produto</th>
                                                <th>Tipo de Produto</th>
                                                <th>Categoria</th>
                                                <th>Validade</th>
                                                <th>Quantidade</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: ACCORDION PANEL -->
    </div>
</div>
@stop

<!-- /.content -->
@section('scripts')
<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/table-data.js')}}"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{('assets/js/main.js')}}"></script>
<script>
    function redirect() {

        var url = "/user/dispesas&receitas/dispesas/nova";
        window.open(url, '_self');
    }

    jQuery(document).ready(function() {
        TableData.init();
    });
</script>
@stop
@stop