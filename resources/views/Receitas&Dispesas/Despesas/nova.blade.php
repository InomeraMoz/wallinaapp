@extends('Dashbord')
@section('conteudo')
<!-- Main content -->

<section class="content">
  <form method="POST" action="{{route('user.dispesas&receitas.dispesas.save')}}" enctype="multipart/form-data">
    @csrf
    <div class="box-footer">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><i class="fa fa-fw fa-pencil-square-o"></i>
          Registo das Dispesas
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Forms</a></li>
          <li class="active">Advanced Elements</li>
        </ol>
      </section>
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Data do registo:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="data">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-5">
              <!-- /.form-group -->
              <div class="form-group">
                <label>Tipo de despesa:</label>
                <select class="form-control select2" style="width: 100%;" name="tipoDespesa">
                  <option selected="selected">Selecione</option>
                  <option>Condóminio</option>
                  <option>Estraórdinaria</option>
                  <option>Imprevistos</option>
                </select>
              </div>
            </div>
            <!-- /.col -->
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>valor/montante:</label>
                <div class="input-group">
                  <span class="input-group-addon">$</span>
                  <input type="text" class="form-control" name="valor">
                  <span class="input-group-addon">.00</span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Textarea</label>
              <textarea class="form-control" rows="2" placeholder="Enter ..." name="Observacao"></textarea>
            </div>
          </div>
          <!-- /.form-group -->
        </div>

        <!-- /.row -->
      </div>
      <!-- /.box-body -->
      <!-- /.box-body -->
      <div class="col-md-3 pull-right pull-left">
        <!-- /.form-group -->
        <div class="form-group">
          <label>total do valor das dispesas do caixa:</label>
          <div class="input-group">
            @foreach ($total as $total)
            <input type="hidden" name="_token" value="{{{csrf_token()}}}">
            <span class="input-group-addon">$</span>
            <div class="form-control input-lg">{{$total->total}}</div>
            <span class="input-group-addon">.00</span>
            @endforeach
          </div>
        </div>
      </div>
      <!-- /.box -->
      <button type="submit" class="btn btn-info pull-right" id="save"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
    </div>
    <!-- /.box -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box-header">
          <h3 class="box-title">Tabela das ultimas Receitas</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="box">
            <!-- <div class="alert alert-danger">
              Não existe nenhuma receita na lista
            </div> -->
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Referência do caixa</th>
                  <th>Data</th>
                  <th>Valor</th>
                  <th>Description</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($dis as $re)
                <tr>
                  <td>{{$re->id}}</td>
                  <td>{{$re->data}}</td>
                  @foreach($cx as $c)
                  @if($c->ContaEmpresa_id===$usuario->contaempresas_id and $c->EstadoCaixa_id===1)
                  <td>{{$c->codCaixa}}</td>
                  @endif
                  @endforeach
                  <td>{{$re->valor}}</td>
                  <td>{{$re->Observacao}}</td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Referência do caixa</th>
                  <th>Data</th>
                  <th>Valor</th>
                  <th>Description</th>
                </tr>
              </tfoot>
            </table>
          </div>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<div class="alert alert-success" id="divForm">
  <strong>Sucesso!</strong> a dispesas foi adicionado.
</div>
<!-- /.content -->

<!-- datepicker -->
@section('scripts')

<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/table-data.js')}}"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{('assets/js/main.js')}}"></script>


<script>
  jQuery(document).ready(function() {
    FormElements.init();
    eventosave();
    TableData.init();
  });
</script>
@stop
@stop