@extends('Dashbord')
@section('stylesheet')


@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Tables de Dados <small>Tabela de Dispesas</small></h1>
  </div>
</div>
@stop
@section('informacao_comp')
<ol class="breadcrumb">
  <li>
    <a href="#">
      Dashboard
    </a>
  </li>
  <li class="active">
    Tabela de Dispesas
  </li>
</ol>
@stop
@section('conteudo')
<div class="row">
  @foreach($total as $total)
  <div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Lista <span class="text-bold">Dispesas</span></h4>
        <div class="panel-tools">
          <div class="dropdown">
            <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
              <i class="fa fa-cog"></i>
            </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li>
                <a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
              </li>
              <li>
                <a class="panel-refresh" href="#">
                  <i class="fa fa-refresh"></i> <span>Refresh</span>
                </a>
              </li>
              <li>
                <a class="panel-config" href="#panel-config" data-toggle="modal">
                  <i class="fa fa-wrench"></i> <span>Configurations</span>
                </a>
              </li>
              <li>
                <a class="panel-expand" href="#">
                  <i class="fa fa-expand"></i> <span>Fullscreen</span>
                </a>
              </li>
            </ul>
          </div>
          <a class="btn btn-xs btn-link panel-close" href="#">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12 space20">
            <button name="redirect" onClick="redirect()" class="btn btn-green add-row" id="redirect">
              Add New <i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-hover" id="sample_2">
            <thead>
              <tr>
                <th>Reference receita</th>
                <th>Date Start</th>
                <th>Referencia do caixa</th>
                <th>valor/montante</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($dispesas as $dispesas)
              <tr>
                <td>{{$dispesas->id}}</td>
                <td>{{$dispesas->data}}</td>
                @foreach($caixa as $c)
                @if($c->ContaEmpresa_id===$usuario->contaempresas_id and $c->EstadoCaixa_id===1)
                <td>{{$c->codCaixa}}</td>
                @endif
                @endforeach
                <td>{{$total->total}}</td>
                <td>
                  <div class="visible-md visible-lg hidden-sm hidden-xs">
                    <a href="{{route('user.dispesas&receitas.dispesas.pegar_id', ['id'=>$dispesas->id])}}" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                    <!-- <a href="#" class="btn btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                    <a href="#" class="btn btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a></i></a> -->
                </td>

              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Reference receita</th>
                <th>Date Start</th>
                <th>Referencia do caixa</th>
                <th>valor/montante</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
@stop

<!-- /.content -->
@section('scripts')
<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/table-data.js')}}"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{('assets/js/main.js')}}"></script>
<script>
  function redirect() {

    var url = "/user/dispesas&receitas/dispesas/nova";
    window.open(url, '_self');
  }

  jQuery(document).ready(function() {
    TableData.init();
  });
</script>
@stop
@stop