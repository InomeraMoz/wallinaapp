@extends('Dashbord_Provide')

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Cliente<small>Formulario de Registo da Empresa</small></h1>
  </div>
</div>
@stop

@section('conteudo')
<!-- Select2 -->
<!-- Main content -->

<!-- Main content -->
@section('informacao1')
<div class="col-sm-6">
  <h1 class="m-0 text-dark">Registo da nova Empresa</h1>
</div><!-- /.col -->
@stop
@section('informacao2')
<li class="breadcrumb-item active">Empresa</li>
@stop

<section class="content">
  <form role="form" action="{{ route('admin.cliente.empresa2.save') }}" enctype="multipart/form-data">
    @csrf
    <div class="container-fluid">
      <div class="card card-default">
        <div class="panel-heading">
        </div>
        <div class="box-footer">
          <!-- SELECT2 EXAMPLE -->
          <div class="card-body">
            <div class="row">


              <div class="col-md-6">
                <div class="form-group">
                  <label>Clientes</label>
                  <select class="form-control select2" style="width: 100%;" name="Cliente_id">
                    <option selected value="{{$cliente->id}}">{{$cliente->nome}}</option>

                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Designação da Empresa</label>
                  <input type="text" class="form-control pull-right" name="designacao">
                </div>
              </div>
            </div>

            <div class="panel-heading">
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Endereço Empresa</label>
                    <div>
                      <!-- <span class="input-group-addon pull-left"> <i class="fa fa-phone"></i> </span> -->
                      <input type="text" class="form-control pull-right" name="endereco_empresa">
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label>contacto</label>
                    <div>
                      <!-- <span class="input-group-addon pull-left"> <i class="fa fa-phone"></i> </span> -->
                      <input type="text" class="form-control pull-right" name="contacto_empresa">
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
      <div class="panel-heading">
      </div>
      <button type="submit" class="btn btn-info pull-right float-right" id="btnsave" float="right"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
      <button type="" class="btn btn-danger pull-left"><i class="fa fa-fw fa-cart-plus"></i>Cancel</button>
    </div>
    <!-- /.box-body -->
    <div class="form-group">

    </div>
  </form>
</section>
<!-- /.content -->

<!-- datepicker -->

@section('scripts')
<script>
  jQuery(document).ready(function() {
    FormElements.init();
  });
</script>
@stop
@stop