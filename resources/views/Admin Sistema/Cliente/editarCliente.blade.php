@extends('Dashbord_Provide')

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Cliente<small>Formulario do Registo do Cliente</small></h1>
  </div>
</div>
@stop

@section('conteudo')
<!-- Select2 -->
<!-- Main content -->

<!-- Main content -->
@section('informacao1')
<div class="col-sm-6">
  <h1 class="m-0 text-dark">Editar dados do cliente</h1>
</div><!-- /.col -->
@stop
@section('informacao2')
<li class="breadcrumb-item active">Cliente</li>
@stop

<section class="content">
  <form role="form" method="POST" action="{{ route('admin.cliente.update',['id'=>$cliente->id])}}" enctype="multipart/form-data">
    @csrf
    <div class="container-fluid">
      <div class="card card-default">
        <div class="panel-heading">
        </div>
        <div class="box-footer">
          <!-- SELECT2 EXAMPLE -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-group">
                    <label>Nome do Cliente_<span name="id"></span></label>
                    <input type="text" class="form-control pull-right" name="nome" value="{{$cliente->nome}}">
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>contacto</label>
                  <div>
                    <!-- <span class="input-group-addon pull-left"> <i class="fa fa-phone"></i> </span> -->
                    <input type="text" class="form-control pull-right" name="contacto" value="{{$cliente->contacto}}">
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Email</label>
                  <div>
                    <!-- <span class="input-group-addon pull-left"> <i class="fa fa-phone"></i> </span> -->
                    <input type="email" class="form-control pull-right" name="email" value="{{$cliente->email}}">
                  </div>
                </div>
              </div>

            </div>

            <div class="panel-heading">
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label>Endereço</label>
                    <div>
                      <!-- <span class="input-group-addon pull-left"> <i class="fa fa-phone"></i> </span> -->
                      <input type="text" class="form-control pull-right" name="endereco" value="{{$cliente->endereco}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
      <div class="panel-heading">
      </div>
      <button type="submit" class="btn btn-info pull-right float-right" id="btnsave" float="right"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
      <!-- <button type="" class="btn btn-danger pull-left"><i class="fa fa-fw fa-cart-plus"></i>Cancel</button> -->
    </div>
 
  </form>
</section>
<!-- /.content -->

<!-- datepicker -->

@section('scripts')
<script>
  jQuery(document).ready(function() {

    //Main.init();
    //SVExamples.init();
    FormElements.init();

    loadpadrao1();
  });
</script>
@stop
@stop