@extends('Dashbord_Provide')

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Utilizador do Sistema<small>Formulario de Registo de utilizador do Sistema</small></h1>
  </div>
</div>
@stop

@section('conteudo')
<!-- Select2 -->
<!-- Main content -->

<!-- Main content -->
@section('informacao1')
<div class="col-sm-6">
  <h1 class="m-0 text-dark">Registo de novo Utilizadors</h1>
</div><!-- /.col -->
@stop
@section('informacao2')
<li class="breadcrumb-item active">Utilizador do Sistema</li>
@stop

<section class="content">


  <div class="col-md-8">
    <div class="panel-heading">
    </div>
    <form class="form-register" method="POST" action="{{ route('admin.cliente.empresa.conta_utilizador.nova') }}">
      @csrf
      <div class="errorHandler alert alert-danger no-display">
        <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
      </div>
      <fieldset>
        <div class="form-group">
          <label>Empresa: </label>
          <select class="form-control select2" style="width: 100%;" name="Empresa_id">
            <option value="">Selecione a empresa</option>
            @foreach($empresa as $ep)
            <option value="{{$ep->id}}">{{$ep->designacao}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label>Tipo de Conta de Utilizador: </label>
          <select class="form-control select2" style="width: 100%;" name="idTipoUsuario">
            <option>Tipo de conta de utilizador</option>
            @foreach($tipoconta as $tp)
            <option value="{{$tp->id}}">{{$tp->designacao}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label>Nome da Conta: </label>
         <input type="text" class="form-control" placeholder="nome Completo" name="nomeConta">
          @error('name')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <label>Validade da conta </label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" name="validade">
          </div>
          @error('name')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <p>
          Abaixo ensira os detalhes da conta/ Enter your account details below:
        </p>
        <div class="form-group">
          <label>Email</label>
          <span class="input-icon">
            <input type="email" class="form-control" name="email" placeholder="Email">
            <i class="fa fa-envelope"></i> </span>
        </div>
        <label>Senha</label>
        <div class="form-group">
          <span class="input-icon">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            <i class="fa fa-lock"></i> </span>
        </div>
        <div class="form-group">
          <span class="input-icon">
            <input type="password" class="form-control" name="password_again" placeholder="Password Again">
            <i class="fa fa-lock"></i> </span>
        </div>
        <div class="form-group">
          <div class="form-group">
            <label>Estado da conta: </label>
            <select class="form-control select2" style="width: 100%;" name="EstadoConta_idEstadoConta">
              <option value="">Selecione o estado da conta</option>
              @foreach($estadoConta as $ec)
              <option value="{{$ec->idEstadoConta}}">{{$ec->designacaoEstado}}</option>
              @endforeach
            </select>
          </div>
          <div>
            <label for="agree" class="checkbox-inline">
              <input type="checkbox" class="grey agree" id="agree" name="agree">
              I agree to the Terms of Service and Privacy Policy
            </label>
          </div>
        </div>
        <div class="form-actions">
          <button type="submit" class="btn btn-green pull-right">
            Submit <i class="fa fa-arrow-circle-right"></i>
          </button>
        </div>
      </fieldset>
    </form>
  </div>


</section>
<!-- /.content -->

<!-- datepicker -->

@section('scripts')
<script>
  jQuery(document).ready(function() {
    FormElements.init();
  });
</script>
@stop
@stop