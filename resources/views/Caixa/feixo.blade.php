@extends('index')
@section('conteudo')
 <!-- Select2 -->
<!-- Main content -->

<section class="content">
<div class="box-footer">
      <!-- Content Header (Page header) -->
      <section class="content-header">
      <h1><i class="fa fa-fw fa-pencil-square-o"></i>
        Registo do Feixo caixa
       </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol> -->
    </section>

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
          </div>
        </div>

        <div class="box-body">
        <div class="row">
        <div class="col-md-4">
              <div class="form-group">

              <div class="form-group">
               <label>Data Feixo:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
              </div>
              </div>
        </div>
        </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">

              <div class="form-group">
                <label>Estado Caixa</label>
                <select class="form-control select2" style="width: 100%;" name="select2">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>

                <!-- <label>Date Start:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div> -->
                <!-- /.input group -->
              </div>

              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <!-- <div class="col-md-3"> 

            <div class="form-group">
            <label>Amount:</label>
            <div class="input-group">
                <input type="number" class="form-control">
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div> -->
            <!-- /.form-group -->
            <!-- </div> -->

            <div class="col-md-5">
              <!-- /.form-group -->
              <div class="form-group">
              <label>Cash:</label>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <span class="form-control"></span>
                <span class="input-group-addon">.00</span>
              </div>
              </div>


             </div>
            <!-- /.col -->
          </div>
              <!-- /.form-group -->
            </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
        <button type="submit" class="btn btn-danger pull-left"><i class="fa fa-fw fa-cart-plus"></i>Cancel</button>
      </div>
      <!-- /.box -->
      <div class="row">
        
        <!-- /.col -->

        
      </div>
      <!-- /.row -->
    </section>
<!-- /.content -->

<!-- datepicker -->




<script>
  function select2() {

    $('.select2').select2()
  }
  function datatable() {
    $('#example1').DataTable()
    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  }
</script>
@stop
