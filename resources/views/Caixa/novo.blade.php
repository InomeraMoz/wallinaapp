@extends('Dashbord')



@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Data Tables <small>Formulario do caixa</small></h1>
  </div>
</div>
@stop
@section('informacao_comp')
<ol class="breadcrumb">
  <li>
    <a href="#">
      Dashboard
    </a>
  </li>
  <li class="active">
    Registo do caixa
  </li>
</ol>
@stop
@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-white">
      <div class="panel-heading">
      </div>
      <div class="panel-body">
        <form role="form" method="POST" class="form-horizontal" action="{{route('user.caixa.abrirCaixa')}}" enctype="multipart/form-data">
          @csrf
          <div class="col-sm-12">
            <div class="col-sm-6">
              <p>
                Date Picker
              </p>
              <div class="input-group">
                <input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control date-picker" name="dataAbertura">
                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
              </div>
              <hr>
            </div>

          </div>
          <!-- start: DATE/TIME PICKER PANEL -->
          <div class="col-sm-12">
            <div class="col-sm-6">
              <div>
                <label for="form-field-mask-5">
                  Valor inicial <small class="text-success">0.00</small>
                </label>
                <div>
                  <input type="text" id="form-field-mask-5" class="form-control currency" name="saldoInicial">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="form-field-select-1">
                  Estado do Caixa
                </label>
                <select id="form-field-select-1" class="form-control" name="estadoCaixaid">
                  <option value="">&nbsp;</option>
                  @foreach ($estadoCaixa as $estadoCaixa)
                  <option value="{{$estadoCaixa->id}}">{{$estadoCaixa->designacao}}</option>
                  @endforeach
                </select>
              </div>

            </div>
          </div>
          <hr>
          <div class="form-group-right">
            <div class="col-sm-2 col-sm-offset-8">
              <button class="btn btn-success finish-step btn-block">
                Save <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@section('scripts')


<script>
  jQuery(document).ready(function() {
    FormElements.init();
  });
</script>
@stop
@stop