@extends('Dashbord')

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Produto<small>Formulario do Registo de produto</small></h1>
  </div>
</div>
@stop

@section('conteudo')
<!-- Select2 -->
<!-- Main content -->

<!-- Main content -->
@section('informacao1')
<div class="col-sm-6">
  <h1 class="m-0 text-dark">Registo do novo produto</h1>
</div><!-- /.col -->
@stop
@section('informacao2')
<li class="breadcrumb-item active">Produtos</li>
@stop


<section class="content">
  <form role="form" method="POST" action="{{route('user.Prduto.save')}}" enctype="multipart/form-data">
    @csrf
    <div class="container-fluid">
      <div class="card card-default">

        <div class="panel-heading">
        </div>
        <div class="box-footer">
          <!-- SELECT2 EXAMPLE -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-group">
                    <label>Designação do produto:</label>
                    <input type="text" class="form-control pull-right" name="Designacao">
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">

                  <div class="form-group">
                    <label>Sub-Designação do produto</label>
                    <input type="text" class="form-control pull-right" name="subDesignacao">
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <div class="form-group">
                    <label>Tamanho</label>
                    <select class="form-control select2" style="width: 100%;" name="tamanho">
                      <option selected="selected">Selecione</option>
                      <option>Pequeno</option>
                      <option>Médio</option>
                      <option>Grande</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">

                    <div class="form-group">
                      <label>Tipo de produto</label>
                      <select class="form-control select2" style="width: 100%;" name="tipoproduto">
                        <option value="">--Selecione--</option>
                        @foreach($tipoProduto as $tipoProduto)
                        <option value="{{$tipoProduto->Designacao}}">{{$tipoProduto->Designacao}}</option>
                        @endforeach
                      </select>
                    </div>

                    <!-- <label>Date Start:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div> -->
                    <!-- /.input group -->
                  </div>

                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Categoria do produto</label>
                    <select class="form-control select2" style="width: 100%;" id="padrao2" name="padrao2" onchange="loadpadrao1()">
                      <option value="">--Selecione--</option>
                      @foreach($padrao2 as $padrao2)
                      <option value="{{$padrao2->id}}">{{$padrao2->designacao2}}</option>
                      @endforeach

                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <div class="col-md-3">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Tipo da Categoria</label>
                    <select class="form-control select2" style="width: 100%;" id="padrao1_id" name="padrao1_id">
                      <option value=""> --Selecione-- </option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Qnd. minima no stouqe</label>
                    <div class="input-group">
                      <input type="number" class="form-control" name="qdaStock">
                      <span class="input-group-addon"><i class="fa fa-check"></i></span>
                    </div>
                  </div>

                  <!-- /.col -->
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.row -->
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <button type="submit" class="btn btn-info pull-right float-right" id="btnsave" float="right"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
      <!-- <button type="" class="btn btn-danger pull-left"><i class="fa fa-fw fa-cart-plus"></i>Cancel</button> -->
    </div>
  </form>
</section>
<!-- /.content -->

<!-- datepicker -->

@section('scripts')
<script>
  function loadpadrao1() {
    $('#padrao2').on('change', function() {
      loadPadrao2(this.value);
    })
  }

  function loadPadrao2(idP) {
    $('#padrao1_id')
      .empty()
      .append('<option selected="selected" value="">--Selecione--</option>');

    @foreach($padrao1 as $padrao1)
    if ({
        {
          $padrao1 - > padrao2_id
        }
      } == idP) {
      $('#padrao1_id')
        .append($("<option></option>")
          .attr("value", "{{$padrao1->id_padrao}}")
          .text("{{$padrao1->designacao_padrao}}"));
    }
    @endforeach
  }

  function prepareEvents() {
    $('#padrao2').on('change', function() {
      loadLocalRegisto(this.value);
    })
  }

  jQuery(document).ready(function() {

    //Main.init();
    //SVExamples.init();
    FormElements.init();

    loadpadrao1();
  });
</script>
@stop
@stop