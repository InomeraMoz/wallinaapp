@extends('Dashbord')
@section('stylesheet')
<link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/styles-responsive.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/themes/theme-style8.css')}}" type="text/css" id="skin_color">
<link rel="stylesheet" href="{{asset('assets/css/print.css')}}" type="text/css" media="print" />

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Configuração <small>Tabela do Tipo do produto/artigo</small></h1>
  </div>
</div>
@stop
@section('informacao_comp')
<ol class="breadcrumb">
  <li>
    <a href="#">
      Dashboard
    </a>
  </li>
  <li class="active">
    Tabela de Tipo de produto/artigo
  </li>
</ol>
@stop
@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-white">
      <div class="panel-heading">
        <div class="panel-tools">
          <div class="dropdown">
            <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
              <i class="fa fa-cog"></i>
            </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li>
                <a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
              </li>
              <li>
                <a class="panel-refresh" href="#">
                  <i class="fa fa-refresh"></i> <span>Refresh</span>
                </a>
              </li>
              <li>
                <a class="panel-config" href="#panel-config" data-toggle="modal">
                  <i class="fa fa-wrench"></i> <span>Configurations</span>
                </a>
              </li>
              <li>
                <a class="panel-expand" href="#">
                  <i class="fa fa-expand"></i> <span>Fullscreen</span>
                </a>
              </li>
            </ul>
          </div>
          <a class="btn btn-xs btn-link panel-close" href="#">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12 space20">
            <button name="redirect" onClick="redirect()" class="btn btn-green add-row" id="redirect">
              Add New <i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Designação</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($tipos as $tipos)
            <tr>
              <td>{{$tipos->idtipoProdutos}}</td>
              <td>{{$tipos->Designacao}}</td>
              <td>
                <div class="visible-md visible-lg hidden-sm hidden-xs">
                  <a href="{{route('user.configuracao.tipoProduto.pegarId', ['id'=>$tipos->id])}}" class="btn btn-light-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                  <a href="#" class="btn btn-green tooltips" data-placement="top" data-original-title="detalhes"><i class="fa fa-share"></i></a>
                  <!-- <a href="#" class="btn btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a> -->
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Designação</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
@stop

<!-- /.content -->
@section('scripts')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/table-data.js')}}"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="{{asset('assets/js/main.js')}}"></script>



<script>
  function redirect() {
    var url = "{{ route('user.configuracao.novoTipo') }}";
    window.open(url, '_self');
  }


  jQuery(document).ready(function() {
    TableData.init();
  });
</script>
@stop
@stop