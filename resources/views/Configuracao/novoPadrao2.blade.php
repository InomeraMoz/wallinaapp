@extends('Dashbord')

@section('informacao1')
<div class="col-sm-6 hidden-xs">
  <div class="page-header">
    <h1>Configuração<small>Formulario do Padrão2</small></h1>
  </div>
</div>
@stop

@section('conteudo')
<!-- Select2 -->
<!-- Main content -->

<!-- Main content -->
@section('informacao1')
<div class="col-sm-6">
  <h1 class="m-0 text-dark">Registo do Padrão 2</h1>
</div><!-- /.col -->
@stop
@section('informacao2')
<li class="breadcrumb-item active">Padrão 2</li>
@stop


<section class="content">
  <form role="form" method="POST" action="{{route('user.configuracao.novoPadrao2.gravar')}}" enctype="multipart/form-data">
    @csrf
    <div class="container-fluid">
      <div class="card card-default">

        <div class="panel-heading">
        </div>
        <div class="box-footer">
          <!-- SELECT2 EXAMPLE -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="form-group">
                    <label>Designação do padrao 2:</label>
                    <input type="text" class="form-control pull-right" name="designacao">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <button type="submit" class="btn btn-info pull-right float-right" id="btnsave" float="right"><i class="fa fa-fw fa-floppy-o"></i>Save</button>
    </div>
  </form>
</section>
<!-- /.content -->

<!-- datepicker -->

@section('scripts')
<script>
  jQuery(document).ready(function() {
    FormElements.init();
  });
</script>
@stop
@stop