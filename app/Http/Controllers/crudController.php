<?php

namespace App\Http\Controllers;

use App\caixa;
use App\cliente;
use App\contaempresa;
use App\dispesas;
use App\empresa;
use App\entradas;
use App\entradasProduto;
use App\estadocaixa;
use App\estadocontas;
use App\padrao1;
use App\padrao2;
use App\produtos;
use App\receitas;
use App\saidas;
use App\saidasProdutos;
use App\stoque;
use App\tipos_produtos;
use App\tiposUsuario;
use App\User;
use Exception;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use SebastianBergmann\Environment\Console;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;

class crudController extends Controller
{
    //

    public function criarCliente(Request $request)
    {
        $cliente = new cliente();
        $cliente->nome = $request->nome;
        $cliente->identificacao = rand();
        $cliente->contacto = $request->contacto;
        $cliente->endereco = $request->endereco;
        $cliente->email = $request->email;
        $cliente->save();

        $cliente = Cliente::all();
        return redirect()->route('admin.cliente.clientes', compact('cliente'));
    }

    protected function updateCliente(Request $request, $id)
    {
        if (Auth::check() === true) {
            $cl = cliente::find($id);
            $cl->nome = $request->nome;
            $cl->contacto = $request->contacto;
            $cl->endereco = $request->endereco;
            $cl->email = $request->email;
            $cl->save();

            $cliente = Cliente::all();
            return redirect()->route('admin.cliente.clientes', compact('cliente'));
        }
        return redirect()->route('admin.login');
    }




    public function criarEmpresa(Request $request)
    {

        $empresa = new empresa();
        $empresa->designacao = $request->designacao;
        $empresa->contacto_empresa = $request->contacto_empresa;
        $empresa->endereco_empresa = $request->endereco_empresa;
        $empresa->Cliente_id = $request->Cliente_id;
        $empresa->save();

        $empresa = empresa::all();
        $cliente = cliente::all();
        $usuario = Auth::user();
        return view('Admin Sistema/Empresa/empresa', compact('empresa', 'cliente', 'usuario'));
    }

    public function criarEmpresa2(Request $request)
    {

        $cliente = cliente::find($request->Cliente_id);
        $empresa = new empresa();
        $empresa->designacao = $request->designacao;
        $empresa->contacto_empresa = $request->contacto_empresa;
        $empresa->endereco_empresa = $request->endereco_empresa;
        $empresa->Cliente_id = $cliente->id;
        $empresa->save();
        $empresa = empresa::all();
        $cliente = cliente::all();
        $usuario = Auth::user();
        return view('Admin Sistema/Empresa/empresa', compact('empresa', 'cliente', 'usuario'));
    }


    public function pegarIdCliente($id)
    {
        if (Auth::check() === true) {
            $cliente = cliente::find($id);
            $usuario = Auth::user();
            return view('Admin Sistema/Empresa/novaEmpresa2', compact('cliente', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    // Metodo para pegar dados dos cliente para actualização de dados


    public function pegarIdCliente_para_actualizacao($id)
    {
        if (Auth::check() === true) {
            $cliente = cliente::find($id);
            $usuario = Auth::user();
            return view('Admin Sistema/Cliente/editarCliente', compact('cliente', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    // Metodo para actualizar dados da empresa

    public function updateEmpresa_id(Request $request, $id)
    {

        if (Auth::check() === true) {
            $emp = empresa::find($id);
            $emp->designacao = $request->designacao;
            $emp->contacto_empresa = $request->contacto_empresa;
            $emp->endereco_empresa = $request->endereco_empresa;
            $emp->Cliente_id = $request->Cliente_id;
            $emp->save();

            $empresa = empresa::all();
            $cliente = cliente::all();
            $usuario = Auth::user();
            return view('Admin Sistema/Empresa/empresa', compact('empresa', 'cliente', 'usuario'));
        }
        return redirect()->route('admin.login');
    }


    // public function mostrarvewEmpresa()
    // {

    //     $cliente = cliente::all();
    //     return view('Admin Sistema/Empresa/novaEmpresa2', compact('cliente'));
    // }


    // //  Metodo para criar conta empresa

    public function criarContaUtilizador(Request $request)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $contaempresa = new contaempresa();
            $contaempresa->nomeUsuario = $request->nomeConta;
            $contaempresa->validade = $request->validade;
            $contaempresa->EstadoConta_idEstadoConta = $request->EstadoConta_idEstadoConta;
            $contaempresa->Empresa_id = $request->Empresa_id;

            $contaempresa->saveOrFail();

            $usuario = new User();
            $usuario->name = $request->nomeConta;
            $usuario->email = $request->email;
            $usuario->password = Hash::make($request->password);
            $usuario->contaempresas_id = $contaempresa->id;
            $usuario->idTipoUsuario = $request->idTipoUsuario;

            $usuario->saveOrFail();

            $contaEmpresa = DB::Select('select * from contaempresas c inner join empresas e inner join estadocontas es where c.Empresa_id=e.id and c.EstadoConta_idEstadoConta=es.idEstadoConta');
            return view('Admin Sistema/Usuario/contasEmpresas', compact('contaEmpresa', 'usuario'));
        }
        return redirect()->route('admin.login');
    }


    // pegando pelo IDEmpresa

    public function criarContaUtilizador2(Request $request)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $empresa = empresa::find($request->Empresa_id);
            $contaempresa = new contaempresa();
            $contaempresa->validade = $request->validade;
            $contaempresa->EstadoConta_idEstadoConta = $request->EstadoConta_idEstadoConta;
            $contaempresa->Empresa_id = $empresa->id;
            $contaempresa->nomeUsuario = $request->nomeConta;

            $contaempresa->saveOrFail();

            $usuario = new User();
            $usuario->name = $request->nomeConta;
            $usuario->email = $request->email;
            $usuario->password = Hash::make($request->password);
            $usuario->contaempresas_id = $contaempresa->id;
            $usuario->idTipoUsuario = $request->idTipoUsuario;

            $usuario->saveOrFail();

            $contaEmpresa = DB::Select('select * from contaempresas c inner join empresas e inner join estadocontas es where c.Empresa_id=e.id and c.EstadoConta_idEstadoConta=es.idEstadoConta');
            return view('Admin Sistema/Usuario/contasEmpresas', compact('contaEmpresa', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    // Metodo para editar conta do utilizador

    public function editarContaUtilizador(Request $request, $id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $users = User::find($id);
            $contaempresa = contaempresa::find($users->contaempresas_id);
            $contaempresa->validade = $request->validade;
            $contaempresa->EstadoConta_idEstadoConta = $request->EstadoConta_idEstadoConta;
            $contaempresa->nomeUsuario = $request->nomeConta;

            $contaempresa->saveOrFail();


            $users->name = $request->nomeConta;
            $users->email = $request->email;
            $users->password = Hash::make($request->password);
            $users->idTipoUsuario = $request->idTipoUsuario;

            $users->saveOrFail();

            $usuario = Auth::user();
            $users = User::all();
            $contaEmpresa = contaempresa::all();
            $empresa = empresa::all();
            $tipoUsuario = tiposUsuario::all();
            $estadoContas = estadocontas::all();

            return redirect()->route('admin.Cliente.Empresa.conta_empresa', compact('contaEmpresa', 'users', 'empresa', 'tipoUsuario', 'estadoContas', 'usuario'));
        }
        return redirect()->route('admin.login');
    }




    // // Metodo para criar todos produtos

    public function criarProduto(Request $request)
    {
        if (Auth::check() === true) {

            $produto = new produtos();
            $produto->Designacao = $request->Designacao;
            $produto->subDesignacao = $request->subDesignacao;
            $produto->tamanho = $request->tamanho;
            $produto->tipoproduto = $request->tipoproduto;
            $produto->qdaStock = $request->qdaStock;
            $produto->padrao1_id = $request->padrao1_id;
            $produto->saveOrFail();

            $stoque = new stoque();
            $stoque->quantidade = 0;
            $stoque->Produtos_id = $produto->id;
            $stoque->saveOrFail();

            $usuario = Auth::user();
            $produtos = produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $tipoProduto = tipos_Produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 = padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao1 = padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return redirect()->route('user.Prduto.produtos', compact('produtos', 'tipoProduto', 'padrao2', 'padrao1', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    // Metodo para actualizar produtos

    public function updateProdutos(Request $request, $id)
    {
        if (Auth::check() === true) {

            $produto = produtos::find($id);
            $produto->Designacao = $request->Designacao;
            $produto->subDesignacao = $request->subDesignacao;
            $produto->tamanho = $request->tamanho;
            $produto->tipoproduto = $request->tipoproduto;
            $produto->qdaStock = $request->qdaStock;
            $produto->padrao1_id = $request->padrao1_id;
            $produto->saveOrFail();

            $usuario = Auth::user();
            $produtos = produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $tipoProduto = tipos_Produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 = padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao1 = padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return redirect()->route('user.Prduto.produtos', compact('produtos', 'tipoProduto', 'padrao2', 'padrao1', 'usuario'));
        }
        return redirect()->route('admin.login');
    }



    // // Configurações  

    public function criarTipoProduto(Request $request)
    {
        if (Auth::check() === true) {
            $tipo = new tipos_produtos();
            $tipo->Designacao = $request->Designacao;
            $tipo->saveOrFail();

            $usuario = Auth::user();
            $tipos = tipos_produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return view('Configuracao.detalhesTipo', compact('tipos', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    public function updatetipoProduto(Request $request, $id)
    {
        if (Auth::check() === true) {
            $tipo = tipos_produtos::find($id);
            $tipo->Designacao = $request->Designacao;

            $tipo->saveOrFail();
            $usuario = Auth::user();
            $tipos = tipos_produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return view('Configuracao.detalhesTipo', compact('tipos', 'usuario'));
        }
        return redirect()->route('admin.login');
    }


    public function gravarPadrao2(Request $request)
    {
        if (Auth::check() === true) {
            $padrao2 = new padrao2();
            $padrao2->designacao2 = $request->designacao;
            $padrao2->saveOrFail();
            $usuario = Auth::user();
            $padrao2 = padrao2::all();
            return redirect()->route('user.configuracao.detalhesPadrao2', compact('padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    public function updatePadrao2(Request $request, $id)
    {
        if (Auth::check() === true) {
            $padrao2 = padrao2::find($id);
            $padrao2->designacao2 = $request->designacao;
            $padrao2->saveOrFail();
            $usuario = Auth::user();
            $padrao2 = padrao2::all();
            return redirect()->route('user.configuracao.detalhesPadrao2', compact('padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    public function gravarPadrao1(Request $request)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $padrao1 = new padrao1();
            $padrao1->padrao2_id = $request->padrao2_id;
            $padrao1->designacao = $request->designacao;
            $padrao1->saveOrFail();

            $padrao1 =  padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 =  padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return redirect()->route('user.configuracao.detalhesPadrao1', compact('padrao1', 'padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    public function updatePadrao1(Request $request, $id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $padrao1 = padrao1::find($id);
            $padrao1->padrao2_id = $request->padrao2_id;
            $padrao1->designacao = $request->designacao;
            $padrao1->saveOrFail();

            $padrao1 =  padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 =  padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return redirect()->route('user.configuracao.detalhesPadrao1', compact('padrao1', 'padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }


    // // Metodo para abrir o caixa ou criar o caixa

    public function abrirCaixa(Request $request)
    {
        $cx = new caixa();
        $usuario = Auth::user();
        $cx->codCaixa = $usuario->id + '2020';
        $cx->dataAbertura = $request->dataAbertura;
        $cx->saldoInicial = $request->saldoInicial;
        $cx->EstadoCaixa_id = $request->estadoCaixaid;
        $cx->ContaEmpresa_id = $usuario->contaempresas_id;
        $cx->saveOrFail();


        $contaEmpresa = contaempresa::find($usuario->contaempresas_id);
        $caixa = caixa::all()->where('ContaEmpresa_id', $contaEmpresa->id);
        $estadoCaixa = estadocaixa::all();
        return redirect()->route('user.caixa.caixas', compact('caixa', 'estadoCaixa', 'usuario'));
    }

    public function update_Caixa(Request $request, $id)
    {
        $cx = caixa::find($id);
        $usuario = Auth::user();
        $cx->dataAbertura = $request->dataAbertura;
        $cx->saldoInicial = $request->saldoInicial;
        $cx->saveOrFail();


        $contaEmpresa = contaempresa::find($usuario->contaempresas_id);
        $caixa = caixa::all()->where('ContaEmpresa_id', $contaEmpresa->id);
        $estadoCaixa = estadocaixa::all();
        return redirect()->route('user.caixa.caixas', compact('caixa', 'estadoCaixa', 'usuario'));
    }


    // // pegar feixo caixa

    // public function pegarFeixoCaixa($id)
    // {
    //     $caixa = caixa::find($id);
    //     return view('Caixa/Caixas', compact('caixa'));
    // }


    // // Metodo para abrir o caixa ou criar o caixa

    public function gravarDespesas(Request $request)
    {
        if (Auth::check() === true) {
            $cx = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $usuario = Auth::user();
            $disp=new dispesas();
            $disp->data = $request->data;
            $disp->tipoDespesa = $request->tipoDespesa;
            $disp->valor = $request->valor;
            $disp->motivo = $request->motivo;
            $disp->Caixa_id = $cx->id;
            $disp->contaempresas_id=$usuario->contaempresas_id;
            $disp->saveOrFail();
            $caixa = caixa::all()->whereIn('contaempresas_id',  $usuario->contaempresas_id);
            $dispesas =  dispesas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            for ($i = 0; $i <= $caixa->count(); $i++) {
                $total = DB::select('select SUM(valor) AS total FROM dispesas where Caixa_id=?', [$i]);
            }
            return view('Receitas&Dispesas.Despesas.detalhes', compact('dispesas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }


    // // Metodo para abrir o caixa ou criar o caixa

    public function gravarReceitas(Request $request)
    {
        if (Auth::check() === true) {
            $cx = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $usuario = Auth::user();
            $rec = new receitas();
            $rec->data = $request->data;
            $rec->valor = $request->valor;
            $rec->Observacao = $request->Observacao;
            $rec->Caixa_id = $cx->id;
            $rec->contaempresas_id = $usuario->contaempresas_id;
            $rec->saveOrFail();
            $caixa = caixa::all()->whereIn('contaempresas_id',  $usuario->contaempresas_id);
            $receitas =  receitas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            for ($i = 0; $i <= $caixa->count(); $i++) {
                $total = DB::select('select SUM(valor) AS total FROM receitas where Caixa_id=?', [$i]);
            }
            return view('Receitas&Dispesas.Receitas.detalhes', compact('receitas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }

    // metodo para actualizar receitas

    public function updateReceitas(Request $request, $id)
    {
        if (Auth::check() === true) {
            $cx = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $usuario = Auth::user();
            $rec = receitas::find($id);
            $rec->data = $request->data;
            $rec->valor = $request->valor;
            $rec->Observacao = $request->Observacao;
            $rec->saveOrFail();
            $caixa = caixa::all()->whereIn('contaempresas_id',  $usuario->contaempresas_id);
            $receitas =  receitas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $total = DB::select('select SUM(valor) AS total FROM receitas r  where r.contaempresas_id=?', [$usuario->contaempresas_id], ' and r.Caixa_id=?', 1);

            return view('Receitas&Dispesas.Receitas.detalhes', compact('receitas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }


    // Metodo para adicionar dispesas

    public function updateDispesas(Request $request, $id)
    {
        if (Auth::check() === true) {
            $cx = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $usuario = Auth::user();
            $dis = dispesas::find($id);
            $dis->data = $request->data;
            $dis->valor = $request->valor;
            $dis->tipoDespesa=$request->tipoDespesa;
            $dis->Observacao = $request->Observacao;            
            $dis->saveOrFail();
            $caixa = caixa::all()->whereIn('contaempresas_id',  $usuario->contaempresas_id);
            $dispesas =  dispesas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $total = DB::select('select SUM(valor) AS total FROM dispesas r  where r.contaempresas_id=?', [$usuario->contaempresas_id], ' and r.Caixa_id=?', 1);

            return view('Receitas&Dispesas.Despesas.detalhes', compact('dispesas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }



    public function createEntradaProduto(Request $request)
    {
        $listaDados = $request->listaDados;
        $lista = explode('"', $listaDados);
        $caixa = caixa::find(1);
        $entrada = new entradas();
        $entrada->dataEntrada = "2020-04-05";
        $entrada->Caixa_id = $caixa->id;
        $entrada->saveOrFail();
        $linhas = explode(";", $lista[1]);
        foreach ($linhas as $linha) {
            $entradaP = new entradasProduto();

            $colunas = explode("|", $linha);

            try {
                $stoque = stoque::all();
                foreach ($stoque as $stoque) {
                    if ($stoque->Produtos_id == $colunas[0]) {
                        $stoque->quantidade += $colunas[3];
                        $stoque->save();
                    }
                }
                $entradaP->Produtos_id = $colunas[0];
                $entradaP->quantidade = $colunas[2];
                $entradaP->precoCompra = $colunas[3];
                $entradaP->desconto = $colunas[4];
                $entradaP->Entradas_id = $entrada->id;
            } catch (Exception $e) {
                report($e);
                return false;
            }
            $entradaP->save();
        }
        $entradas = DB::Select('SELECT * FROM entradas e inner join caixas c where e.Caixa_id=c.id');
        $stotal = null;
        foreach ($entradas as $item) {
            $stotal = DB::select('select SUM(precoCompra*quantidade) AS total FROM entradaprodutos where Entradas_id=?', [$item->idEntrada]);
        }

        return view('Entradas & Saidas/Entradas/Entradas', compact('entradas', 'stotal'));
    }




    // Metodo para gravar as saidas do produto


    public function createSaidaProduto(Request $request)
    {
        $listaDados = $request->listaDados;
        $lista = explode('"', $listaDados);
        $caixa = caixa::find(1);
        $saida = new saidas();
        $saida->dataSaida = date('yy-m-d');
        $saida->Caixa_id = $caixa->id;
        $saida->saveOrFail();
        $linhas = explode(";", $lista[1]);
        foreach ($linhas as $linha) {
            $saidaP = new saidasProdutos();

            $colunas = explode("|", $linha);

            try {
                $stoque = stoque::all();
                foreach ($stoque as $stoque) {
                    if ($stoque->Produtos_id == $colunas[0]) {
                        $stoque->quantidade -= $colunas[2];
                        $stoque->save();
                    }
                }
                $saidaP->Produtos_id = $colunas[0];
                $saidaP->quantidade = $colunas[2];
                $saidaP->precoVenda = $colunas[3];
                $saidaP->desconto = $colunas[4];
                $saidaP->Saida_id = $saida->id;
            } catch (Exception $e) {
                report($e);
                return false;
            }
            $saidaP->save();
        }

        $saidas = DB::Select('SELECT * FROM saidas s inner join caixas c where s.caixa_id=c.id');
        $total = 0;
        foreach ($saidas as $item) {
            $total = DB::select('select SUM((precoCompra*quantidade)-desconto) AS total FROM saidaprodutos where Saida_id=?', [$item->id]);
        }

        return view('Entradas & Saidas/Saidas/Saidas', compact('saidas', 'total'));
    }
}
