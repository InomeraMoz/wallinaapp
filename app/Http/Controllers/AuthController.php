<?php

namespace App\Http\Controllers;

use App\contaempresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function dashbord()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            if ($usuario->idTipoUsuario === 1) {
               // toast('bem vindo ao sistema', $usuario->name);
                return view('Dashbord', compact('usuario'));
            }
           // toast('bem vindo ao sistema', $usuario->name);
            return view('Dashbord_Provide', compact('usuario'));
        } else {
            return redirect()->route('admin.login');
        }
    }

    public function showforLogin()
    {

        return view('auth.loggin');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            return redirect()->route('admin');
        } else {

            return redirect()->back();
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin');
    }
}
