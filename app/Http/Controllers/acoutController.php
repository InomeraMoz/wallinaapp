<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class acoutController extends Controller
{
    //


    protected function validacao(array $data){
        return validator::make($data,[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'Empresa_id'=>['required', 'int', 'min:1'],
            'idTipoUsuario'=>['required', 'int', 'min:1'],
            'Empresa_id'=>['required', 'int', 'min:1'],
            'validade'=>['required', 'Date'],
            'EstadoConta_idEstadoConta'=>['required', 'int', 'min:1'],
        ]);
    }

    protected function createAcount(array $data){
        
    }
}
