<?php

namespace App\Http\Controllers;

use App\caixa;
use App\cliente;
use App\contaempresa;
use App\dispesas;
use App\empresa;
use App\entradas;
use App\entradasProduto;
use App\estadocaixa;
use App\estadocontas;
use App\padrao1;
use App\padrao2;
use App\produtos;
use App\receitas;
use App\saidas;
use App\stoque;
use App\tipos_produtos;
use App\tiposUsuario;
use App\User;
use Error;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class indexController extends Controller
{
    //

    protected function getcaixa()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $contaEmpresa = contaempresa::find($usuario->contaempresas_id);
            $caixa = caixa::all()->where('ContaEmpresa_id', $contaEmpresa->id);
            $estadoCaixa = estadocaixa::all();
            return view('Caixa.Caixas', compact('caixa', 'estadoCaixa', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getnewcaixa()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $estadoCaixa = estadocaixa::all();
            return view('Caixa.Novo', compact('estadoCaixa', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegarId_Caixa($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::find($id);
            $estadoCaixa = estadocaixa::find($caixa->EstadoCaixa_id);
            if ($caixa->EstadoCaixa_id === 1) {
                return view('Caixa.editNovo', compact('caixa', 'estadoCaixa', 'usuario'));
            }

            return redirect()->back()->withInput()->withErrors(['Não pode editar um caixa feixado']);
        }
        return redirect()->route('admin.login');
    }

    //     protected function getfeixocaixa()
    //     {
    //         return view('Caixa/feixo');
    //     }

    protected function getdetalhesReceitas()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all();
            $receitas =  receitas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);

            $total = DB::select('select SUM(valor) AS total FROM receitas r  where r.contaempresas_id=?', [$usuario->contaempresas_id], ' and r.Caixa_id=?', 1);

            return view('Receitas&Dispesas.Receitas.detalhes', compact('receitas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }

    protected function getReceitas()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('ContaEmpresa_id', $usuario->contaempresas_id);
            $receitas =  receitas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);

            $arr_length = count($caixa);
            for ($i = 0; $i < $arr_length; $i++) {
                $total = DB::select('select SUM(valor) AS total FROM receitas where Caixa_id=?', [$i]);
            }

            return view('Receitas&Dispesas.Receitas.detalhes', compact('receitas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }


    // Metodo para pegar dados das receitas

    protected function pegar_id_receitas($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $receitas =  receitas::find($id);
            $rec = receitas::all()->whereIn('Caixa_id', 1)->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $cx = caixa::all();
            // $cx=caixa::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $total = DB::select('select SUM(valor) AS total FROM receitas where Caixa_id=?', [$caixa->id], 'and contaempresas_id=?', [$usuario->contaempresas_id]);
            return view('Receitas&Dispesas/Receitas/editarReceitas', compact('receitas', 'total', 'usuario', 'caixa', 'rec', 'cx'));
        }
        return redirect()->route('admin.login');
    }


    protected function getdetalhesDispesas()
    {

        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('ContaEmpresa_id', $usuario->contaempresas_id);
            $dispesas =  dispesas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);

            $arr_length = count($caixa);
            for ($i = 0; $i < $arr_length; $i++) {
                $total = DB::select('select SUM(valor) AS total FROM dispesas where Caixa_id=?', [$i]);
            }

            return view('Receitas&Dispesas.Despesas.detalhes', compact('dispesas', 'total', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }


    protected function getDispesas()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $dis = dispesas::all()->whereIn('Caixa_id', 1)->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $cx = caixa::all();
            // $cx=caixa::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $total = DB::select('select SUM(valor) AS total FROM dispesas where Caixa_id=?', [$caixa->id]);
            return view('Receitas&Dispesas/Despesas/nova', compact('total', 'usuario', 'caixa', 'dis', 'cx'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegar_id_dispesas($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();
            $dispesas =  dispesas::find($id);
            $dis = dispesas::all()->whereIn('Caixa_id', 1)->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $cx = caixa::all();
            // $cx=caixa::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $total = DB::select('select SUM(valor) AS total FROM dispesas where Caixa_id=?', [$caixa->id]);
            return view('Receitas&Dispesas/Despesas/editarDispesas', compact('dispesas', 'totao', 'usuario', 'caixa', 'dis', 'cx'));
        }
        return redirect()->route('admin.login');
    }

    protected function getSaidas()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();

            $saidas = saidas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id)->whereIn('Caixa_id', $caixa->id);
            $stotal = null;
            foreach ($saidas as $item) {
                $stotal = DB::select('select SUM(precoVenda*quantidade) AS total FROM saidaprodutos where Saida_id=?', [$item->id]);
            }

            return view('Entradas&Saidas/Saidas/Saidas', compact('saidas', 'stotal', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }

    protected function getEntradas()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $caixa = caixa::all()->whereIn('EstadoCaixa_id', 1)->first();

            $entradas = entradas::all()->whereIn('contaempresas_id', $usuario->contaempresas_id)->whereIn('Caixa_id', $caixa->id);
            $stotal = null;
            foreach ($entradas as $item) {
                $stotal = DB::select('select SUM(precoCompra*quantidade) AS total FROM entradaprodutos where Entradas_id=?', [$item->idEntrada]);
            }

            return view('Entradas&Saidas/Entradas/Entradas', compact('entradas', 'stotal', 'usuario', 'caixa'));
        }
        return redirect()->route('admin.login');
    }

    protected function getDetalhesPadrao1()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();

            $padrao1 =  padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 =  padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return view('Configuracao.detalhesPadrao1', compact('padrao1', 'padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getnovoPadrao1()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $padrao2 = padrao2::all();
            return view('Configuracao/novoPadrao1', compact('padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegarnovoPadrao1($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $padrao1 =  padrao1::find($id);
            $padrao2 =  padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $pa2 = padrao2::find($padrao1->padrao2_id);
            return view('Configuracao.editPadrao1', compact('padrao1', 'padrao2', 'pa2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getDetalhesPadrao2()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $padrao2 = padrao2::all();
            return view('Configuracao.detalhesPardao2', compact('padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getnovoPadrao2()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            return view('Configuracao.novoPadrao2', compact('usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegarnovoPadrao2($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $padrao2 = padrao2::find($id);
            return view('Configuracao.editarPadrao2', compact('padrao2', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getDetalhesTipo()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $tipos = tipos_produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return view('Configuracao.detalhesTipo', compact('tipos', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegarId_tipoProduto($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $tipos = tipos_produtos::find($id);
            return view('Configuracao.editarTipo', compact('tipos', 'usuario'));
        }
        return redirect()->route('admin.login');
    }




    protected function getnovoTipo()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();

            return view('Configuracao.novoTipo', compact('usuario'));
        }
        return redirect()->route('admin.login');
    }



    protected function getContasEmpresas()
    {
        if (Auth::check() === true) {

            $usuario = Auth::user();
            $users = User::all();
            $contaEmpresa = contaempresa::all();
            $empresa = empresa::all();
            $tipoUsuario = tiposUsuario::all();
            $estadoContas = estadocontas::all();

            return view('Admin Sistema/Usuario/contasEmpresas', compact('contaEmpresa', 'users', 'empresa', 'tipoUsuario', 'estadoContas', 'usuario'));
        }

        return redirect()->route('admin.login');
    }

    //     protected function getnovoContaEmpresa()
    //     {
    //         $empresas = empresa::all();
    //         $estadoconta = estadoconta::all();
    //         return view('Admin Sistema/Usuario/novaContaEmpresa', compact('empresas', 'estadoconta'));
    //     }

    protected function getCliente()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $cliente = cliente::all();
            return view('Admin Sistema.Cliente.clientes', compact('cliente', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getnovoCliente()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            return view('Admin Sistema/Cliente/novoCliente', compact('usuario'));
        }
        return redirect()->route('admin.login');
    }


    protected function getEmpresa()
    {
        if (Auth::check() === true) {

            $cliente = cliente::all();
            $empresa = empresa::all();
            $usuario = Auth::user();
            return view('Admin Sistema/Empresa/empresa', compact('empresa', 'cliente', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getEmpresabyId($id)
    {
        if (Auth::check() === true) {
            $empresa = empresa::find($id);
            $usuario = Auth::user();
            $clie = cliente::find($empresa->Cliente_id);
            $clientes = cliente::all();
            return view('Admin Sistema/Empresa/editarEmpresa', compact('clientes', 'clie', 'empresa', 'usuario'));
        }
        return redirect()->route('admin.login');
    }


    protected function getnovaEmpresa()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $clientes = cliente::all();
            return view('Admin Sistema/Empresa/novaEmpresa', compact('clientes', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegarIdEmpresa($id)
    {
        if (Auth::check() === true) {
            $empresa = empresa::find($id);
            $tipoconta = tiposUsuario::all();
            $estadoConta = estadocontas::all();
            $usuario = Auth::user();
            return view('Admin Sistema/Usuario/novaContaUsuario2', compact('empresa', 'tipoconta', 'estadoConta', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getContaUsuario()
    {
        if (Auth::check() === true) {
            $empresa = empresa::all();
            $tipoconta = tiposUsuario::all();
            $estadoConta = estadocontas::all();
            $usuario = Auth::user();
            return view('Admin Sistema/Usuario/novaContaUsuario', compact('empresa', 'tipoconta', 'estadoConta', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function pegar_id_Utilizador($id)
    {
        if (Auth::check() === true) {
            $users = User::find($id);
            $contaEmpresa = contaempresa::find($users->contaempresas_id);
            $empresa = empresa::find($contaEmpresa->Empresa_id);
            $emp = empresa::all();
            $tp = tiposUsuario::find($users->idTipoUsuario);
            $esc = estadocontas::find($contaEmpresa->EstadoConta_idEstadoConta);
            $tipoconta = tiposUsuario::all();
            $estadoConta = estadocontas::all();
            $usuario = Auth::user();
            return view('Admin Sistema/Usuario/editarContaUsuario', compact('empresa', 'contaEmpresa',  'users', 'tp', 'esc', 'emp', 'tipoconta', 'estadoConta', 'usuario'));
        }
        return redirect()->route('admin.login');
    }





    //     protected function getOperation()
    //     {
    //         return view('Operation');
    //     }

    protected function getEntradaProduto()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $produtos = produtos::all();
            return view('Entradas&Saidas/Entradas/EntradaProduto', compact('produtos', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getSaidaProduto()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $produtos = produtos::all();
            $item = $produtos;
            return view('Entradas&Saidas/Saidas/SaidaProduto', compact('produtos', 'item', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getnovoProduto()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $tipoProduto = tipos_Produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 = padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao1 = padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            return view('Produto.novoProduto', compact('tipoProduto', 'padrao2', 'padrao1', 'usuario'));
        }
    }

    protected function pegar_id_produto($id)
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            $produtos = DB::select('select pr.*, p2.designacao2 , p1.designacao_padrao  FROM produtos pr INNER JOIN padrao2s p2 INNER JOIN padrao1s p1 WHERE pr.padrao1_id=p1.id_padrao AND p1.padrao2_id=p2.id AND pr.id=?', [$id]);
            $tipoProduto = tipos_produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 = padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao1 = padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            foreach ($produtos as $pro) {
                $p1 = padrao1::all()->whereIn('id_padrao', $pro->padrao1_id)->first();
            }
            $p2 = padrao2::find($p1->padrao2_id);

            return view('Produto.editarProduto', compact('produtos', 'p1', 'p2', 'tipoProduto', 'padrao2', 'padrao1', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    protected function getProdutos()
    {
        if (Auth::check() === true) {
            $usuario = Auth::user();
            // $produtos = produtos::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao2 = padrao2::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $padrao1 = padrao1::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            // foreach ($padrao2 as $p2) {
            $produtos = DB::select('select pr.*, p2.designacao2 , p1.designacao_padrao  FROM produtos pr INNER JOIN padrao2s p2 INNER JOIN padrao1s p1 WHERE pr.padrao1_id=p1.id_padrao AND p1.padrao2_id=p2.id AND pr.contaempresas_id=?', [$usuario->contaempresas_id]);
            // }
            return view('Produto.produtos', compact('produtos', 'padrao2', 'padrao1', 'usuario'));
        }
        return redirect()->route('admin.login');
    }

    //     function redirect_back_or_default()
    //     {
    //         try {
    //             return Redirect::back();
    //         } catch (Exception $e) {
    //             return Redirect::to('/home');
    //         }
    //     }
    //

    protected function getprodutosStoque()
    {

        if (Auth::check() === true) {
            $usuario = Auth::user();
            $produtos = DB::select('SELECT * FROM stoques s INNER JOIN produtos p iNNER JOIN padrao1s p1s WHERE s.Produtos_id=p.id AND p.padrao1_id=p1s.id_padrao AND p.contaempresas_id=?', [$usuario->contaempresas_id]);
            $stoque = stoque::all()->whereIn('contaempresas_id', $usuario->contaempresas_id);
            $entradas = DB::select('SELECT * FROM entradaprodutos e inner join produtos p inner join padrao1s p1s where e.Produtos_id=p.id and p.padrao1_id=p1s.id_padrao and DATEDIFF(e.validade, now())>=60 and p.contaempresas_id=?', [$usuario->contaempresas_id]);
            return view('stoque.produtosStoque', compact('usuario', 'produtos', 'stoque', 'entradas'));
        }
        return redirect()->route('admin.login');
    }
}
