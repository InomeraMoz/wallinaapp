<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class apiRequestController extends Controller
{
    //


    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (Auth::attempt($credentials)) {
            $usuarioLogin=Auth::user();
            return response()->jason($usuarioLogin);
        }
        return Response()->json("erro de validação login", 200);
    }
}
